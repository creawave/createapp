import React, {useState} from 'react';
import './Header.sass'
import { NavLink } from "react-router-dom";
import routes from "../../../../routes";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

const Header = () => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <div className={'header'}>
            <div className='header-menu'>
                <button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} className='form-add'>
                    Open Menu
                </button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem><NavLink exact={true} activeClassName={'header-active'} className={'header-link'} to={routes.home} > Home </NavLink></MenuItem>
                    <MenuItem><NavLink activeClassName={'header-active'} className={'header-link'} to={routes.about} > About </NavLink></MenuItem>
                    <MenuItem><NavLink activeClassName={'header-active'} className={'header-link'} to={routes.contacts} > Contacts </NavLink></MenuItem>
                    <MenuItem><NavLink activeClassName={'header-active'} className={'header-link'} to={routes.newContacts} > New Contacts </NavLink></MenuItem>
                </Menu>
            </div>
            <div className={'header-logo'}>
                YOUR <span>LOGO</span>
            </div>
            <nav className={'header-nav'}>
                <NavLink exact={true} activeClassName={'header-active'} className={'header-link'} to={routes.home} > Home </NavLink>
                <NavLink activeClassName={'header-active'} className={'header-link'} to={routes.about} > About </NavLink>
                <NavLink activeClassName={'header-active'} className={'header-link'} to={routes.contacts} > Contacts </NavLink>
                <NavLink activeClassName={'header-active'} className={'header-link'} to={routes.newContacts} > New Contacts </NavLink>
            </nav>
        </div>
    )
};

export default Header;