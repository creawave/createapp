const routes = {
    home: '/',
    about: '/about',
    newContacts: '/new-contacts',
    contacts: '/contacts',
};

export default routes;