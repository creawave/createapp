import React from 'react';
import './About.sass';
import about from '../../../assets/img/about.png'

const About = () => {
    return (
        <div className={'about'}>
            <h1>ABOUT</h1>
            <p className={'about-subtitle'}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
            </p>
            <div className={'about-img'}>
                <img src={about} alt="#"/>
            </div>
        </div>
    )
};

export default About;