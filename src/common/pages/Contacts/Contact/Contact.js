import React, {useState} from "react";
import './Contact.sass';
import {useDispatch} from "react-redux";
import {changeReadonly, deleteContact, renameContact} from "../../../../store/actions/contact";

const Contact = ({ id, firstName, secondName, phone, readonly}) => {
    const dispatch = useDispatch();
    const [name, setName] = useState(firstName);
    const [telephone, setTelephone] = useState(phone);
    const [sename, setSename] = useState(secondName);

    const abortRename = () => {
        dispatch(changeReadonly(id));
        setName(firstName);
    };

    const changeInformation = () => {
        dispatch(changeReadonly(id));
        dispatch(renameContact(name, sename, telephone, id))
    };

    return (
        <div className='card'>
            <div className='card-text'>
                Имя:
                <input type="text"
                       readOnly={readonly}
                       value={name}
                       className={readonly ? 'card-input' : 'card-input-active'}
                       onChange={({target:{ value }}) => setName(value)}
                />
            </div>
            <div className='card-text'>
                Фамилия:
                <input type="text"
                       readOnly={readonly}
                       value={sename}
                       className={readonly ? 'card-input' : 'card-input-active'}
                       onChange={({target:{ value }}) => setSename(value)}
                />
            </div>
            <div className='card-text'>
                Телефон:
                <input type="text"
                       readOnly={readonly}
                       value={telephone}
                       className={readonly ? 'card-input' : 'card-input-active'}
                       onChange={({target:{ value }}) => setTelephone(value)}
                />
            </div>
            <div className='card-buttons'>
                {
                    readonly && <button className='form-add' onClick={() => dispatch(changeReadonly(id))}>Edit</button>
                }
                {
                    !readonly && <div className='card-buttons'>
                        <button className='form-add' onClick={changeInformation}>Ok</button>
                        <button className='form-clear' onClick={abortRename}>Back</button>
                    </div>
                }
                <button className='form-clear' onClick={() => {
                    console.log(id);
                    dispatch(deleteContact(id));
                }}>Delete</button>
            </div>
        </div>
    )
};

export default Contact;