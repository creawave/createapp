import React, { useState } from 'react';
import './Contacts.sass';
import {useSelector} from "react-redux";
import Contact from "./Contact/Contact";

const Contacts = () => {
    const contacts = useSelector(state => state.contactsReducer.contacts);
    const [value, setValue] = useState('');
    return (
        <div className={'contacts'}>
            <h1>YOUR CONTACTS</h1>
            <div className='contacts-search'>
                <input type="text" placeholder='Enter contact name' value={value} onChange={({target:{ value }}) => setValue(value)}/>
            </div>
            <div className={'contacts-wrapper'}>
                {
                    contacts.filter(item => item.firstName.toLowerCase().includes(value.toLowerCase())).map(({firstName, secondName, phone, id, readonly}) => {
                        return (
                            <Contact id={id}
                                     key={id}
                                     firstName={firstName}
                                     secondName={secondName}
                                     phone={phone}
                                     readonly={readonly}
                            />
                        )
                    })
                }
            </div>
        </div>
    )
};

export default Contacts;