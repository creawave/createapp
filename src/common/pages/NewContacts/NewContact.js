import React, {useState} from 'react';
import './NewContact.sass';
import contact from '../../../assets/img/contact.jpg';
import {addNewContact, changeVisible} from "../../../store/actions/contact";
import {Field, reduxForm, reset} from "redux-form";
import {renderField, validate} from "../../../Validation";
import {useDispatch, useSelector} from "react-redux";

let NewContacts = ({handleSubmit, reset}) => {
    const dispatch = useDispatch();
    const visible = useSelector(state => state.contactsReducer.visible);
    let submit = (values) => {
        dispatch(addNewContact(values));
        dispatch(reset('post'));
    };
    return (
        <div className={'contact'}>
            <div className={'contact-box'}>
                <h1 className={'form-title'}>NEW CONTACT</h1>
                <form onSubmit={handleSubmit(submit)} className={'form'}>
                    <label className='form-label'>
                        First Name:
                        <Field name="firstName"  component={renderField} type="text"/>
                    </label>
                    <label className={'form-label'}>
                        Second Name:
                        <Field name="secondName"  component={renderField} type="text"/>
                    </label>
                    <label className={'form-label'}>
                        Phone:
                        <Field name="phone" component={renderField} type="text"/>
                    </label>
                    <div className={'form-button'}>
                        <button type="button"
                                onClick={reset}
                                className={'form-clear'}>Clear form</button>
                        <button type="submit"
                                className={'form-add'}
                                onClick={() => submit}>Add contact</button>
                    </div>
                </form>
                {
                    visible &&
                    <div className='form-modal'>
                        <p>Contact added successfully!</p>
                        <button className='form-clear' onClick={() => dispatch(changeVisible())}>OK</button>
                    </div>
                }
            </div>
            <div className={'contact-box'}>
                <img src={contact} alt="#"/>
            </div>
        </div>

    )
};


NewContacts = reduxForm({
    form: 'post',
    validate,
})(NewContacts);

export default NewContacts;