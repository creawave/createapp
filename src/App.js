import React from 'react';
import './App.sass';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Header from "./common/components/global/Layout/Header/Header";
import Home from "./common/pages/Home/Home";
import About from "./common/pages/About/About";
import routes from "./common/routes";
import NewContacts from "./common/pages/NewContacts/NewContact";
import Contacts from "./common/pages/Contacts/Contacts";

const App = () => {
  return (
      <BrowserRouter>
        <div className={'app-wrapper'}>
          <Header/>
          <div className={'app-content'}>
              <Switch>
                  <Route exact path={routes.home} render={ () => <Home/> }/>
                  <Route path={routes.about} render={ () => <About/> }/>
                  <Route path={routes.newContacts} render={ () => <NewContacts/> }/>
                  <Route path={routes.contacts} render={ () => <Contacts/> }/>
              </Switch>
          </div>
        </div>
      </BrowserRouter>
  )
};




export default App;
