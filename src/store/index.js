import React from "react";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { reducer as formReducer } from 'redux-form';
import {contactsReducer} from "./reducers/contact";

const rootReducers = combineReducers({
    contactsReducer,
    form: formReducer,

});

const middleware = [thunk];
const middlewareEnhancer = applyMiddleware(...middleware);

const store = createStore(rootReducers, composeWithDevTools(middlewareEnhancer));



export default store;