import {
    ADD_NEW_CONTACT,
    CHANGE_READONLY,
    CHANGE_VISIBLE,
    DELETE_CONTACT,
    RENAME_CONTACT
} from "../actionsTypes/contact";

const initialState = {
    contacts: [],
    visible: false,
};

export const contactsReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ADD_NEW_CONTACT:
            const id = state.contacts.length + 1;
            return {
                ...state,
                visible: true,
                contacts: [...state.contacts, {...payload, id, readonly: true}]
            };
        case DELETE_CONTACT:
            return {
                ...state,
                contacts: payload,
            };
        case CHANGE_READONLY:
            return  {
                ...state,
                contacts: payload,
            };
        case RENAME_CONTACT:
            return {
                ...state,
                contacts: payload,
            };
        case CHANGE_VISIBLE:
            return {
                ...state,
                visible: false,
            };
        default:
            return state;
    }
};