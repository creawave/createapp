import {
    ADD_NEW_CONTACT,
    CHANGE_READONLY,
    CHANGE_VISIBLE,
    DELETE_CONTACT,
    RENAME_CONTACT
} from "../actionsTypes/contact";

export const addNewContact = form => ({
    type: ADD_NEW_CONTACT,
    payload: form,
});

export const changeVisible = () => ({
    type: CHANGE_VISIBLE,
});

export function deleteContact(id) {
    return (dispatch, getState) => {
        const {contactsReducer: {contacts: newContacts}} = getState();
        dispatch({
            type: DELETE_CONTACT,
            payload: newContacts.filter(el => el.id !== id),
        });
    }
}

export const changeReadonly = id => {
    return (dispatch, getState) => {
        const {contactsReducer: {contacts: newContacts}} = getState();
        return dispatch({
            type: CHANGE_READONLY,
            payload: newContacts.map(item => {
                if(item.id === id) {
                    item.readonly = !item.readonly;
                }
                return item;
            })

        })
    }
};

export const renameContact = (newFirstName,newSecondName, newPhone, id) => {
    return (dispatch, getState) => {
        const {contactsReducer: {contacts: newContacts}} = getState();
        dispatch({
            type: RENAME_CONTACT,
            payload: newContacts.map(item => {
                if(item.id === id) {
                    item.firstName = newFirstName;
                    item.secondName = newSecondName;
                    item.phone = newPhone;
                }
                return item;
            })
        });
    }
};