import React from 'react';

export const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Required'
    } else if (values.firstName.length > 15) {
        errors.firstName = 'Must be 15 characters or less'
    }
    if (!values.secondName) {
        errors.secondName = 'Required'
    } else if (values.secondName.length > 15) {
        errors.secondName = 'Must be 15 characters or less'
    }
    if(!values.phone) {
        errors.phone = 'Required'
    }
    return errors
};


export const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span className={'wrapper-error'}>{error}</span>) || (warning && <span className={'wrapper-error'}>{warning}</span>))}
        </div>
    </div>);